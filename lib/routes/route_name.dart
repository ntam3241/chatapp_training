class RouteName {
  static const String initial = '/';
  static const String chat = '/chat';
  static const String login = '/login';
  static const String signUp = '/signup';
}
