import 'package:flutter/material.dart';

class AppColors {
  ///main color app
  static const Color lightThemeColor = Color(0xFFFFFFFF);
  static const Color darkThemeColor = Color(0xFF1C1C1C);
  static const Color subLightColor = Colors.grey;
  static const Color subdarkColor = Color(0xDD000000);
  static const Color primaryColor = Colors.blueAccent;
  static const Color secondaryColor = Color(0xFF90CAF9);

  static const listBgColor = [
    Color(0xFFFF9800),
    Color(0xFF00BFFF),
    Color(0xFFC8E6C9),
    Color(0xFFFF69B4),
  ];

  static const Color backgroundCommonColor = Color(0xFFFFF3E0);
  static const Color backgroundSearchColor = Color(0xFFEEEEEE);
  static const Color backgroundEditColor = Color(0xFFE0E0E0);

  static const Color errorColor = Colors.red;
  static const Color likeColor = Colors.red;
  static const Color sussess = Colors.green;

  /// button
  static const Color disibleColor = Color(0xFFEF6C00);
}
