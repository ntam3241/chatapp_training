import 'dart:ui';

import 'package:chat_app/until/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppTextField {
  static common({
    String? hintText,
    TextStyle? hintStyle,
    FocusNode? focusNode,
    TextEditingController? controller,
    String? errorText,
    InputBorder? boder,
    Function(String input)? onChanged,
    bool isPassword = false,
  }) {
    return TextField(
      focusNode: focusNode,
      controller: controller,
      textInputAction: TextInputAction.done,
      cursorHeight: 20.0,
      cursorColor: AppColors.primaryColor,
      obscureText: isPassword,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: hintStyle,
        errorText: errorText,
        border: boder,
      ),
      onChanged: onChanged,
    );
  }
}
