import 'package:firebase_auth/firebase_auth.dart';

class AppFireBaseAuthService {
  final auth = FirebaseAuth.instance;

  Future<bool> login(String email, String password) async {
    try {
      return await auth
          .signInWithEmailAndPassword(email: email, password: password)
          .then((user) => true);
    } catch (e) {
      return false;
    }
  }

  Future<bool> signUp(String email, String password) async {
    try {
      return await auth
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((user) => true);
    } catch (e) {
      return false;
    }
  }

  Future<bool> signOut() async {
    try {
      await auth.signOut().whenComplete(() => true);
      return true;
    } catch (e) {
      return false;
    }
  }
}
