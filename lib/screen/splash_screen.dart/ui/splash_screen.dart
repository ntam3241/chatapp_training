import 'package:chat_app/routes/route_name.dart';
import 'package:chat_app/theme.dart';
import 'package:chat_app/until/app_color.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(
      const Duration(seconds: 1),
      () => Navigator.pushReplacementNamed(context, RouteName.login),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              AppColors.secondaryColor,
              AppColors.primaryColor,
              AppColors.primaryColor,
            ],
          ),
        ),
        child: const Center(
          child: Text(
            'Chat App',
            style: TextStyle(
              fontSize: 30.0,
              fontWeight: AppFontWeight.extraBold,
              color: AppColors.lightThemeColor,
            ),
          ),
        ),
      ),
    );
  }
}
