import 'package:flutter/material.dart';

class ChatScreen extends StatefulWidget {
  const ChatScreen({Key? key}) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              theme.backgroundColor,
              theme.backgroundColor,
              const Color(0xFFE6E6E6),
            ],
          ),
        ),
        child: Column(
          children: [
            Flexible(
              child: ListView.builder(
                itemCount: 50,
                itemBuilder: (_, index) => const Text('aaaaa'),
              ),
            ),
            Row(
              children: const [
                Expanded(
                  child: TextField(),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
